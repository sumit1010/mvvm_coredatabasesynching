
import Foundation

struct StringContant {
    
    public static var userApiUrl = "https://dummyjson.com/users?limit=5&skip=10&select=firstName,age,lastName"
    public static var coreDataEntityName = "Users"
    
    public static var id = "id"
    public static var firstName = "firstName"
    public static var lastName = "lastName"
    public static var Age = "age"
}
