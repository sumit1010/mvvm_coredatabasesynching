
import UIKit


//MARK: Api success/ failure protocol
protocol UserListViewModelDelegate {
    func getAllUsersList(arrayOfUser: [User]?)
}

class UserListViewModel {
    var delegate: UserListViewModelDelegate?
    var arrayOfUser: [User] = []

    init() {
        print("LoginViewModel initialized...")
    }
    
    //MARK: Api To get User From Api
    func callApiToGetUserData() {
        
        if Reachablity.isConnectedToNetwork() {
            print("Internet is available...")
            
            ApiHandler.getApiCall(serverUrl: StringContant.userApiUrl, for: UsersDataModel.self) {  result in
                switch result {
                case .success(let response):
                    DispatchQueue.main.async {
                        if response.users.count > 0 {
                            DataBaseHandler.addRecordToDataBase(userModelArray: response.users)
                            self.updateApiResponseHandlerDelegateMethod(arrayOfUser: response.users)
                        } else {
                            print("No Data found")
                            self.updateApiResponseHandlerDelegateMethod(arrayOfUser: nil)
                        }
                    }
                case .failure(let error):
                    print("Login api response is: \(error.localizedDescription)")
                    self.updateApiResponseHandlerDelegateMethod(arrayOfUser: nil)
                }
            }
        } else  {
            let allUsers = DataBaseHandler.fetchRecordsFromDataBase()
            guard let allUsers = allUsers else {
                updateApiResponseHandlerDelegateMethod(arrayOfUser: nil)
                return
            }
            if allUsers.count > 0 {
                self.updateApiResponseHandlerDelegateMethod(arrayOfUser: allUsers)
                
            }
        }
    }
    
    func updateApiResponseHandlerDelegateMethod(arrayOfUser: [User]?) {
        guard let arrayOfUser = arrayOfUser else { return }
        if arrayOfUser.count > 0 {
            self.delegate?.getAllUsersList(arrayOfUser: arrayOfUser)
        } else {
            self.delegate?.getAllUsersList(arrayOfUser: nil)
        }
    }
}


