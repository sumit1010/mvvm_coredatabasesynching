 import Foundation


// MARK: - User
struct UsersDataModel: Codable {
    let users: [User]
    let total, skip, limit: Int
}

struct User: Codable {
    let id: Int
    let lastName: String
    let firstName: String
    let age: Int
}
