 
import UIKit

class UserCell: UITableViewCell {
    
    @IBOutlet weak var lbl_id: UILabel!
    @IBOutlet weak var lbl_firstName: UILabel!
    @IBOutlet weak var lbl_lastName: UILabel!
    @IBOutlet weak var lbl_age: UILabel!
    
    var userObject: User? {
        didSet {
            lbl_id.text = "\(userObject?.id ?? 0)"
            lbl_firstName.text = userObject?.firstName
            lbl_lastName.text = userObject?.lastName
            lbl_age.text = "\(userObject?.age ?? 0)"
        }
    }
  }
