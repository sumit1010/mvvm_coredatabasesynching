//
//  ViewController.swift
//  CoreDataWithLiveDBSync
//
//  Created by Sumit Vishwakarma on 19/05/23.
//

import UIKit

class UserListVC: UIViewController {
    
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    @IBOutlet weak var tableViewOfUsers: UITableView!
//    var arrayOfUser: [User] = []
    let userListVM = UserListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeUserInterFace()
    }
    
    private func initializeUserInterFace() {
        userListVM.delegate = self
        self.tableViewOfUsers.rowHeight = 150.0
        self.activityLoader.startAnimating()
        self.activityLoader.isHidden = false
        userListVM.callApiToGetUserData()
    }
}

//MARK: UserList to show User Data
extension UserListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userListVM.arrayOfUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UserCell
        cell.userObject = userListVM.arrayOfUser[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(userListVM.arrayOfUser[indexPath.row].firstName)
    }
    
}

extension UserListVC: UserListViewModelDelegate {
    
    func getAllUsersList(arrayOfUser: [User]?) {
        self.activityLoader.isHidden = true
        guard let allUsers = arrayOfUser else {
            return
        }
        if allUsers.count > 0 {
            DispatchQueue.main.async {
                self.userListVM.arrayOfUser = allUsers
                self.tableViewOfUsers.reloadData()
             }
        }
    }
}
