
import UIKit
import CoreData


//MARK: -- DataBase Handler To Handle Data Localy --
class DataBaseHandler {
    
    static let persistentContainer = PersistentStorage.sharedInstance.persistentContainer
    
    //MARK: Add User to Data Base
    static func addRecordToDataBase(userModelArray: [User]) {
        
        self.deleteAllUserFromDataBase() // -- Delete All Users from dataBase
        let managedObjectContext = persistentContainer.viewContext
        for user in userModelArray {
            let userDB = Users(context: managedObjectContext)
            userDB.id         = Int16(user.id)
            userDB.firstName  = user.firstName
            userDB.lastName   = user.lastName
            userDB.age        = Int16(user.age)
        }
        do {
            try managedObjectContext.save()
            print("All Records Saved Successfully")
        } catch {
            print("Unable to Save users, \(error)")
        }
    }
    
    //MARK: -- Fetch all Records from DB --
    static func fetchRecordsFromDataBase() -> [User]? {
        
        var userArray:[User] = []
        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: StringContant.coreDataEntityName)
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            for objects in results {
                let object = objects as! NSManagedObject
                let user = User(id: object.value(forKey: StringContant.id) as? Int ?? 0,
                                lastName: object.value(forKey: StringContant.lastName) as? String ?? "",
                                firstName: object.value(forKey: StringContant.firstName) as? String ?? "",
                                age: object.value(forKey: StringContant.Age) as? Int ?? 0)
                userArray.append(user)
            }
            do {
                try managedContext.save()
                print("All Records fetched from DB")
                return userArray
            }
            catch {
                print(error)
                return nil
            }
        }
        catch {
            return nil
        }
    }
    
    //MARK: -- Delete all Records from DB --
    static func deleteAllUserFromDataBase() {
        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: StringContant.coreDataEntityName)
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            for objects in results {
                let objectToDelete = objects as! NSManagedObject
                managedContext.delete(objectToDelete)
            }
            do {
                try managedContext.save()
                print("All Records Deleted from DB")
            }
            catch {
                print(error)
                print("Error in deletion...")
            }
        }
        catch {
            print(error)
        }
    }
}


