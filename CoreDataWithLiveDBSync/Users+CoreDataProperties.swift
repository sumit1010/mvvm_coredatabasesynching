//
//  Users+CoreDataProperties.swift
//  CoreDataWithLiveDBSync
//
//  Created by Sumit Vishwakarma on 22/05/23.
//
//

import Foundation
import CoreData


extension Users {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Users> {
        return NSFetchRequest<Users>(entityName: "Users")
    }

    @NSManaged public var age: Int16
    @NSManaged public var firstName: String?
    @NSManaged public var id: Int16
    @NSManaged public var lastName: String?

}

extension Users : Identifiable {

}
